package start;

import presentation.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The class for testing the application
 */
public class MainClass {
    protected static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());

    /**
     * The method which triggers the application to run
     * @param args the name of the .txt file in which we can find commands for performing operations in the database
     */
    public static void main(String[] args){
       try{
           InputParser parser = new InputParser(args[0]);
           parser.read();
       } catch(Exception e){
           LOGGER.log(Level.WARNING, e.getMessage());
       }
    }
}
