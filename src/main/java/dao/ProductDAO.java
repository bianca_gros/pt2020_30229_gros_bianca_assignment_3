package dao;

import model.Product;
/**
 * Class which extends AbstractDAO class, inheriting all its methods
 */
public class ProductDAO extends AbstractDAO<Product> {

}
