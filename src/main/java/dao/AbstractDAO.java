package dao;

import java.beans.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.*;

/**
 * Parametrized class defining all the data access operations
 * @param <T> any type between Client, Product, Order or OrderItem
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    /**
     * Constructs a new instance of the class initializing the type value with the name of the class which replaces the T parameter
     */
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Creates a list with all the elements from the result set, namely the fields of the specified type
     * @param resultSet the given result set
     * @return an ArrayList with the created elements
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Builds a String representing a SQL SELECT statement
     * @param field the field wwhich will appear in the WHERE condition of the SQL SELECT statement
     * @return the string representing the statement
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " = ?");
        return sb.toString();
    }

    /**
     * Builds a String representing a SQL SELECT * statement
     * @return the string representing the statement
     */
    private String createReportQuery(){
        //SELECT * FROM tableName
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("*");
        sb.append(" FROM warehouse.");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    /**
     * Builds a String representing a SQL INSERT statement
     * @return the string representing the statement
     */
    private String createInsertQuery(){
        // INSERT INTO tableName VALUES(?,?,...,?)
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO warehouse.");
        sb.append(type.getSimpleName());
        if(type.getSimpleName().equalsIgnoreCase("client")){
            sb.append(" VALUES (?,?,?)");
        }
        if(type.getSimpleName().equalsIgnoreCase("product")){
            sb.append( " VALUES(?,?,?,?)");
        }
        if(type.getSimpleName().equalsIgnoreCase("orderitem")){
            sb.append(" VALUES (?,?,?)");
        }
        if(type.getSimpleName().equalsIgnoreCase("order")){
            sb.append(" VALUES (?,?,?)");
        }
        return sb.toString();
    }

    /**
     * Builds a String representing a SQL UPDATE statement
     * @param columnToUpdate the column to which we want to make updates
     * @return the string representing the statement
     */
    private String createUpdateQuery(String columnToUpdate){
        // UPDATE tableName SET columnToUpdate = ? WHERE client/product name = ?
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET " + columnToUpdate + " = ? ");
        sb.append(" WHERE ");
        if(type.getSimpleName().equalsIgnoreCase("client")){
            sb.append("clientName = ?");
        }
        if(type.getSimpleName().equalsIgnoreCase("product")){
            sb.append("productName = ?");
        }
        return sb.toString();
    }

    /**
     * Builds a String representing a SQL DELETE statement
     * @param condition the condition which will appear in the WHERE clause of the statement
     * @return the string representing the statement
     */
    private String createDeleteQuery(String condition){
        // DELETE FROM tableName WHERE condition = ?
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + condition + " = ?");
        return sb.toString();
    }

    /**
     * Finds a specific item from the database by id
     * @param id the id upon which we will search
     * @return the element with that id
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "";
        if(type.getSimpleName().equalsIgnoreCase("client")){
            query = createSelectQuery("clientId");
        }
        if(type.getSimpleName().equalsIgnoreCase("product")){
            query = createSelectQuery("productId");
        }
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Finds a specific item from the database by name
     * @param name the name with which we will search for the element
     * @return the element with that name
     */
    public T findByName(String name) { //by name for product and client, by id for order and orderitem
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "";
        if(type.getSimpleName().equalsIgnoreCase("Client")){
            query = createSelectQuery("clientName");
        }
        else if (type.getSimpleName().equalsIgnoreCase("Product")){
            query = createSelectQuery("productName");
        }
        else {
            query = createSelectQuery("OrderId");
        }

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creates and returns a list of elements executing the SELECT * query in the database
     * @return the list of the elements
     */
    public List<T> report() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createReportQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:report " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Inserts a given item into the database
     * @param t the item to insert
     * @return the inserted item
     */
    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i = 1;
            for(Field f : type.getDeclaredFields()){
                Field aux = f;
                aux.setAccessible(true);
                T n = (T) aux.get(t);
                statement.setObject(i++,n);
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * Updates a specific row in a specific table from tha database
     * @param t the type of the table in which we want to make updates
     * @param upd a name or a value, depending on the type on which the operation is performed
     * @param whereCondition the condition used to search for the row to update
     * @return the type upon which the insertion is operated
     */
    public T update(T t, Object upd, Object whereCondition) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "";
        if(type.getSimpleName().equalsIgnoreCase("client")){
            query = createUpdateQuery("clientName");
        }
        if(type.getSimpleName().equalsIgnoreCase("product")){
            query = createUpdateQuery("productQuantity");
        }
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            if(type.getSimpleName().equalsIgnoreCase("client")){
                statement.setString(1, (String)upd);
                statement.setString(2, (String)whereCondition);
            }
            if(type.getSimpleName().equalsIgnoreCase("product")) {
                statement.setInt(1, (int)upd);
                statement.setString(2, (String)whereCondition);
            }

            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * Deletes a specific row from the database
     * @param t the type representing the table from which we want to delete items
     * @param name the name searched to be deleted
     * @return the deleted object
     */
    public T delete(T t, String name){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "";
        if(type.getSimpleName().equalsIgnoreCase("client")){
            query = createDeleteQuery("clientName");
        }
        if(type.getSimpleName().equalsIgnoreCase("product")){
            query = createDeleteQuery("productName");
        }
        /*else{
            query = createDeleteQuery("orderId");
        }*/
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            if(type.getSimpleName().equalsIgnoreCase("client")){
                statement.setString(1, name);
            }
            if(type.getSimpleName().equalsIgnoreCase("product")) {
                statement.setString(1, name);
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return t;
    }

}
