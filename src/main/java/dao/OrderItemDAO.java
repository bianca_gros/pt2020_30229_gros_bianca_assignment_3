package dao;

import model.OrderItem;
/**
 * Class which extends AbstractDAO class, inheriting all its methods
 */
public class OrderItemDAO extends AbstractDAO<OrderItem> {

}
