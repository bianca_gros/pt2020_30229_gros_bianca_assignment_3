package dao;

import model.Order;
/**
 * Class which extends AbstractDAO class, inheriting all its methods
 */
public class OrderDAO extends AbstractDAO<Order> {
}
