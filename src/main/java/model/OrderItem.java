package model;

/**
 * Class for the order table from the database
 */
public class OrderItem {
    private int orderId;
    private int productId;
    private int orderQuantity;

    /**
     * Constructs an orderItem with a given order id, a product id and a quantity of that product
     * @param orderId the order's id
     * @param productId the id of the product which is in the present order
     * @param orderQuantity the quantity of the product from the order
     */
    public OrderItem(int orderId, int productId, int orderQuantity) {
        this.orderId = orderId;
        this.productId = productId;
        this.orderQuantity = orderQuantity;
    }

    /**
     * Constructs an orderItem being given a product id for which the order is placed and the product's quantity
     * @param productId the product's id
     * @param orderQuantity the quantity of the product
     */
    public OrderItem(int productId, int orderQuantity) {
        this.productId = productId;
        this.orderQuantity = orderQuantity;
    }

    /**
     * Empty constructor
     */
    public OrderItem(){

    }

    /**
     * Retrieves the order id for a specific order
     * @return the order's id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Sets an order's id with a specific value
     * @param orderId the value of the order's id
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Retrieves the product's id form the current order
     * @return the value of the product's id
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the product's id from an order with a specific value
     * @param productId the product's id to set
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Retrieves the quantity of the product for which the order was placed
     * @return the quantity of the product
     */
    public int getOrderQuantity() {
        return orderQuantity;
    }

    /**
     * Sets the product's quantity from the current order with a specific value
     * @param orderQuantity the product's quantity to set
     */
    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    /**
     * Method to have a literal value as a  representation of the object of that class
     * @return a string representing the object OrderItem
     */
    @Override
    public String toString() {
        return "OrderItem{" +
                "orderId=" + orderId +
                ", productId=" + productId +
                ", orderQuantity=" + orderQuantity +
                '}';
    }
}
