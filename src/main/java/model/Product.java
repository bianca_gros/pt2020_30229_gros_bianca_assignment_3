package model;

/**
 * Class representing the product table from the database
 */
public class Product {
    private int productId;
    private String productName;
    private int productQuantity;
    private float productPrice;

    /**
     * Empty constructor
     */
    public Product(){

    }

    /**
     * Constructs a Product object being given a name, a quantity and a price
     * @param productName the product's name
     * @param productQuantity the product's quantity
     * @param productPrice the product's price
     */
    public Product(String productName, int productQuantity, float productPrice) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    /**
     * Retrieves the product's id
     * @return the value of the product's id
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the product's id with a specific value
     * @param productId the product's id
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Retrieves the product's name
     * @return the name of the product
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the name of the product with a specific value
     * @param productName the name of the product
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Retrieves the product's quantity
     * @return the value of the product's quantity
     */
    public int getProductQuantity() {
        return productQuantity;
    }

    /**
     * Sets the product's quantity to a specific vqlue
     * @param productQuantity the product's quantity
     */
    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    /**
     * Retrieves the product's price
     * @return the value of the product's price
     */
    public float getProductPrice() {
        return productPrice;
    }

    /**
     * Sets the price of a product with a specific value
     * @param productPrice the product price to set
     */
    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * Method to have a literal value as a  representation of the object of that class
     * @return a string representing a Product object
     */
    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productQuantity=" + productQuantity +
                ", productPrice=" + productPrice +
                '}';
    }
}
