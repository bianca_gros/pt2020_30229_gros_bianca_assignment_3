package model;

/**
 * Class for the Client table form the database, consisting of three fields: clientId, clientName and clientAddress
 */
public class Client {
    private int clientId;
    private String clientName;
    private String clientAddress;

    /**
     * Empty constructor
     */
    public Client(){

    }

    /**
     * Constructs a Client with a name and an address
     * @param clientName the client's name
     * @param clientAddress the client's address
     */
    public Client(String clientName, String clientAddress) {
        this.clientName = clientName;
        this.clientAddress = clientAddress;
    }

    /**
     * Constructs a client with only the name
     * @param clientName the client's name
     */
    public Client(String clientName) {
        this.clientName = clientName;
        }

    /**
     * Retrieves the client id's
     * @return the clientId value
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Sets a client's id with a given value
     * @param clientId the client's id
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Retrieves the client's name
     * @return the clientName value
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Sets the client's name with a given name
     * @param clientName the client's name
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * Retrieves the client's address
     * @return the client's address
     */
    public String getClientAddress() {
        return clientAddress;
    }

    /**
     * Sets the client's address to a specific location
     * @param clientAddress the client's address
     */
    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    /**
     * Method to have a literal value as a  representation of the object of that class
     * @return the String representing a Client object
     */
    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", clientName='" + clientName + '\'' +
                ", clientAddress='" + clientAddress + '\'' +
                '}';
    }
}
