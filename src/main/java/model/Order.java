package model;

/**
 * Class representing the order table from the database
 */
public class Order {
    private int orderId;
    private int clientId;
    private float orderTotal;

    /**
     * Constructs an order with a given id, a given client id and a given total
     * @param orderId order's id
     * @param clientId the id of the client which placed the order
     * @param orderTotal the total of the order
     */
    public Order(int orderId, int clientId, float orderTotal) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.orderTotal = orderTotal;
    }

    /**
     * Empty constructor
     */
    public Order(){

    }

    /**
     * Retrieves the order's id
     * @return the value of the order's id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Sets the order's id with the specified value
     * @param orderId the value of the id
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Retrieves the id of the client which placed the order
     * @return the value of the client's id
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Sets the client's id with the specified value
     * @param clientId the client's id to set
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Retrieves the order's total
     * @return the value of the total for a specific order
     */
    public float getOrderTotal() {
        return orderTotal;
    }

    /**
     * Sets the order total with a specific value
     * @param orderTotal the total of the order
     */
    public void setOrderTotal(float orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * Method to have a literal value as a representation of the object of that class
     * @return a string representing a Order object
     */
    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", clientId=" + clientId +
                ", orderTotal=" + orderTotal +
                '}';
    }
}
