package bll;

import dao.OrderDAO;
import model.Order;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class used for bussiness access layer together with the OrderDAO class, reimplementing or quarding dao operations
 */
public class OrderBll {
    private OrderDAO order;

    /**
     * Constructs a new OrderBll by initializing the OrderDAO object
     */
    public OrderBll(){
        this.order  = new OrderDAO();
    }

    /**
     * Finds an order by id
     * @param id the id to search for
     * @return the found order
     */
    public Order findById(int id){
        List<Order> foundOrders = order.report();
        for(Order i : foundOrders){
            if(i.getOrderId() == id){
                return i;
            }
        }
        throw new NoSuchElementException("The order with id=" + id + " was not found!");
    }

    /**
     * Inserts the given order into the database
     * @param o the order to insert
     * @return the inserted order
     */
    public Order insert(Order o){
        List<Order> foundOrders = order.report();
        for(Order i : foundOrders){
            if(i.getOrderId() == o.getOrderId() && i.getClientId() == o.getClientId() && i.getOrderTotal() == o.getOrderTotal()){
                try {
                    throw new Exception("The order to insert is already in the database!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        order.insert(o);
        return o;
    }

    /**
     * Deletes the given order from the database
     * @param o the order to delete
     * @return the deleted order
     */
    public Order delete(Order o){
        List<Order> foundOrders = order.report();
        for(Order i : foundOrders){
            if(i.getOrderId() == o.getOrderId()){
                return order.delete(o, o.getOrderId() + "");
            }
        }
        throw new NoSuchElementException("The order to be deleted was not found!");
    }

    /**
     * Reports all the orders from the database
     * @return a List of Orders
     */
    public List<Order> report(){
        return order.report();
    }
}
