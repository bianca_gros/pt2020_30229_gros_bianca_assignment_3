package bll;

import dao.ProductDAO;
import model.Product;

import java.util.List;
import java.util.NoSuchElementException;
/**
        * Class used for bussiness access layer together with the ProductDAO class, reimplementing or quarding dao operations
        */
public class ProductBll {
    private ProductDAO product;

    /**
     * Constructs a new ProductBll by initializing the ProductDAO object
     */
    public ProductBll(){
        this.product = new ProductDAO();
    }

    /**
     * Finds a specific product by id
     * @param id the id to search for
     * @return the found product
     */
    public Product findById(int id){
        List<Product> foundProducts = product.report();
        for(Product i : foundProducts){
            if(i.getProductId() == id){
                return i;
            }
        }
        throw new NoSuchElementException("The product with id=" + id + " was not found!");
    }

    /**
     * Finds a specific product by name
     * @param name the name to search for
     * @return the found product
     */
    public Product findByName(String name){
        List<Product> foundProducts = product.report();
        for(Product i : foundProducts){
            if(i.getProductName().equals(name)){
                return i;
            }
        }
        throw new NoSuchElementException("The product with the name of " + name + " was not found!");
    }

    /**
     * Inserts the given product into the database
     * @param p the product to insert
     * @return the inserted product
     */
    public Product insert(Product p){
        int flag = 0;
        List<Product> foundProducts = product.report();
        for(Product i : foundProducts){
            if(i.getProductName().equals(p.getProductName())){
                product.update(i, i.getProductQuantity() + p.getProductQuantity(), p.getProductName());
                flag = 1;
                return product.update(p, p.getProductQuantity() + i.getProductQuantity(), (product.findByName(p.getProductName())).getProductName());
            }
        }
        if(flag == 0){
            product.insert(p);
        }
        return p;
    }

    /**
     * Updates the quantity of a specific product from the database
     * @param p the product to update
     * @param upd the new quantity
     * @param whereCondition the condition for the WHERE clause
     * @return the updated product
     */
    public Product update(Product p, Object upd, Object whereCondition){
        List<Product> foundProducts = product.report();
        for(Product i : foundProducts){
            if(i.getProductName().equals(p.getProductName())){
                return product.update(p, upd, whereCondition);
            }
        }
        throw new NoSuchElementException("The product to be updated was not found!");
    } // upd = cantitatea deja updatata care i se va asocia de acum respectivului produs

    /**
     * Deletes a specific product from the database
     * @param p the product to delete
     * @param name the name of the product to delete
     * @return the deleted product
     */
    public Product delete(Product p, String name){
        List<Product> foundProducts = product.report();
        for(Product i : foundProducts){
            if(i.getProductName().equals(p.getProductName())){
                return product.delete(p, p.getProductName());
            }
        }
        throw new NoSuchElementException("The product to be deleted was not found!");
    }

    /**
     * Reports all the products from the database
     * @return a List of Products
     */
    public List<Product> report(){
        return product.report();
    }
}
