package bll;

import dao.ClientDAO;
import model.Client;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class used for bussiness access layer together with the ClientDAO class, reimplementing or quarding dao operations
 */
public class ClientBll {
    private ClientDAO client;

    /**
     * Constructs a new ClientBll by initializing the ClientDAO object
     */
    public ClientBll(){
        this.client = new ClientDAO();
    }

    /**
     * Finds a specific client by id
     * @param id the id to search for
     * @return the found client
     */
    public Client findById(int id){
        List<Client> foundClients = client.report();
        for(Client i : foundClients){
            if(i.getClientId() == id){
                return i;
            }
        }
        throw new NoSuchElementException("The client with id=" + id + " was not found!");
    }

    /**
     * Finds a specific client by name
     * @param name the name to search for
     * @return the found client
     */
    public Client findByName(String name){
        List<Client> foundClients = client.report();
        for(Client i : foundClients){
            if(i.getClientName().equals(name)){
                return i;
            }
        }
        throw new NoSuchElementException("The client with the name of " + name + " was not found!");
    }

    /**
     * Inserts a client into the database
     * @param c the client to be inserted into the database
     * @return the inserted client
     */
    public Client insert(Client c){
        List<Client> foundClients = client.report();
        for(Client i : foundClients){
            if(i.getClientName().equals(c.getClientName()) && (i.getClientAddress().equals(c.getClientAddress()))){
                try {
                    throw new Exception("The client to insert is already in the database!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        client.insert(c);
        return c;
    }

    /**
     * Updates the name of a specific client from the database
     * @param c the client to pe updated
     * @param upd the new name
     * @param whereCondition the condition for the WHERE clause
     * @return the updated client
     */
    public Client update(Client c, Object upd, Object whereCondition){
        List<Client> foundClients = client.report();
        for(Client i : foundClients){
            if(i.getClientName().equals(c.getClientName()) && (i.getClientAddress().equals(c.getClientAddress()))){
                return client.update(c, upd, whereCondition);
            }
        }
        throw new NoSuchElementException("The client to be updated was not found!");
    }

    /**
     * Deletes a specific client from the database
     * @param c the client to be deleted
     * @param name the client's name
     * @return the deleted client
     */
    public Client delete(Client c, String name){
        List<Client> foundClients = client.report();
        for(Client i : foundClients){
            if(i.getClientName().equals(c.getClientName()) && (i.getClientAddress().equals(c.getClientAddress()))){
                return client.delete(c, c.getClientName());
            }
        }
        throw new NoSuchElementException("The client to be deleted was not found!");
    }

    /**
     * Reports all the clients from the database
     * @return a List of Clients
     */
    public List<Client> report(){
        return client.report();
    }

}
