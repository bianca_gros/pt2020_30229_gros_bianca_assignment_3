package bll;

import dao.OrderItemDAO;
import model.OrderItem;

import java.util.List;
import java.util.NoSuchElementException;
/**
 * Class used for bussiness access layer together with the OrderItemDAO class, reimplementing or quarding dao operations
 */
public class OrderItemBll {
    private OrderItemDAO orderItem;

    /**
     * Constructs a new OrderItemBll by initializing the OrderItemDAO object
     */
    public OrderItemBll(){
        this.orderItem = new OrderItemDAO();
    }

    /**
     * Finds a specific order item by id
     * @param id the id to search for
     * @return the found order
     */
    public OrderItem findById(int id){
        List<OrderItem> foundOrderItems = orderItem.report();
        for(OrderItem i : foundOrderItems){
            if(i.getOrderId() == id){
                return i;
            }
        }
        throw new NoSuchElementException("The order item with id=" + id + " was not found!");
    }

    /**
     * Inserts the given order item into the database
     * @param o the order item to insert
     * @return the inserted order item
     */
    public OrderItem insert(OrderItem o){
        List<OrderItem> foundOrderItems = orderItem.report();
        for(OrderItem i : foundOrderItems){
            if(i.getOrderId() == o.getOrderId()){
                try {
                    throw new Exception("The order item to insert is already in the database!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        orderItem.insert(o);
        return o;
    }

    /**
     * Deletes the given order item from the database
     * @param o the order item to delete
     * @return the deleted order item
     */
    public OrderItem delete(OrderItem o){
        List<OrderItem> foundOrderItems = orderItem.report();
        for(OrderItem i : foundOrderItems){
            if(i.getOrderId() == o.getOrderId()){
                return orderItem.delete(o, o.getOrderId() + "");
            }
        }
        throw new NoSuchElementException("The orderItem to be deleted was not found!");
    }

    /**
     * Reports all order items from the database
     * @return a List of OrderItems
     */
    public List<OrderItem> report(){
        return orderItem.report();
    }
}
