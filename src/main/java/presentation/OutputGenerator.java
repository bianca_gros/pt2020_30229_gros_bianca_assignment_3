package presentation;

import bll.ClientBll;
import bll.OrderBll;
import bll.OrderItemBll;
import bll.ProductBll;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Stream;

/**
 * Class for generating the outpup in the pdf format
 */
public class OutputGenerator {
    private Document document;
    private PdfPTable clientTable;
    private PdfPTable productTable;
    private PdfPTable orderTable;

    /**
     * Constructs a new OutputGenerator by initializing its document
     */
    public OutputGenerator(){
        this.document = new Document();
    }

    /**
     * Creates the header for the report on the Client's table
     * @param table the table on which the header will be
     */
    private void addClientTableHeader(PdfPTable table) {
        Stream.of("Client Id", "Client Name", "Client Address")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * Creates the header for the report on the Product's table
     * @param table the table on which the header will be
     */
    private void addProductTableHeader(PdfPTable table) {
        Stream.of("Product Id", "Product Name", "Product Quantity", "Product Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * Creates the header for the report on the Order's table
     * @param table the table on which the header will be
     */
    private void addOrderTableHeader(PdfPTable table) {
        Stream.of("Order Id", "Client Name", "Ordered Item", "Quantity", "Price/unit", "Total")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * Adds the desired content in the cells of the Client table
     * @param table the table in which the rows will be added
     */
    private void addRowsClientTable(PdfPTable table) {
        ClientBll cBll = new ClientBll();
        List<Client> clienti = cBll.report();
        for(Client i : clienti){
            table.addCell(i.getClientId()+"");
            table.addCell(i.getClientName()+"");
            table.addCell(i.getClientAddress()+"");
        }
    }

    /**
     * Adds the desired content in the cells of the Product table
     * @param table the table in which the rows will be added
     */
    private void addRowsProductTable(PdfPTable table) {
        ProductBll pBll = new ProductBll();
        List<Product> produse = pBll.report();
        for(Product i : produse){
            table.addCell(i.getProductId()+"");
            table.addCell(i.getProductName()+"");
            table.addCell(i.getProductQuantity()+"");
            table.addCell(i.getProductPrice()+"");
        }
    }

    /**
     * Adds the desired content in the cells of the table representing a specific order
     * @param table the table in which the rows will be added
     * @param idOrder the id of the order
     */
    private void addRowsOrderTable(PdfPTable table, int idOrder) {
        OrderItemBll oiBll = new OrderItemBll();
        OrderBll oBll = new OrderBll();
        ClientBll cBll = new ClientBll();
        ProductBll pBll = new ProductBll();
        OrderItem i = oiBll.findById(idOrder);
        table.addCell(i.getOrderId()+"");
        Order o = oBll.findById(i.getOrderId());
        Client cl = cBll.findById(o.getClientId());
        table.addCell(cl.getClientName()+"");
        OrderItem it = oiBll.findById(i.getOrderId());
        Product p = pBll.findById(it.getProductId());
        table.addCell(p.getProductName());
        table.addCell(i.getOrderQuantity()+"");
        table.addCell(p.getProductPrice()+"");
        table.addCell(o.getOrderTotal()+"");
    }

    /**
     * Creates and writes the content of the Client table to the pdf file
     */
    public void writeClientTable() {
        try {
            this.clientTable = new PdfPTable(3);
            PdfWriter.getInstance(document, new FileOutputStream("ClientTable.pdf"));
            document.open();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addClientTableHeader(clientTable);
        addRowsClientTable(clientTable);
        try {
            document.add(new Paragraph("\n\n"));
            Chunk title = new Chunk("Clients");
            document.add(title);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(clientTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    /**
     * Creates and writes the content of the Product table to the pdf file
     */
    public void writeProductTable() {
        try {
            this.productTable = new PdfPTable(4);
            PdfWriter.getInstance(document, new FileOutputStream("ProductTable.pdf"));
            document.open();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addProductTableHeader(productTable);
        addRowsProductTable(productTable);
        try {
            document.add(new Paragraph("\n\n"));
            Chunk title = new Chunk("Products");
            document.add(title);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(productTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    /**
     * Creates and writes the content of a specific order to the pdf file
     * @param idOrder the given order id
     */
    public void writeOrderTable(int idOrder) {
        ClientBll cBll = new ClientBll();
        OrderItemBll oiBll = new OrderItemBll();
        OrderItem oi = oiBll.findById(idOrder);
        OrderBll oBll = new OrderBll();
        Order o = oBll.findById(idOrder);
        Client c = cBll.findById(oBll.findById(o.getOrderId()).getClientId());
        try {
            this.orderTable = new PdfPTable(6);
            PdfWriter.getInstance(document, new FileOutputStream("OrderNo" + idOrder + ".pdf"));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addOrderTableHeader(orderTable);
        addRowsOrderTable(orderTable, idOrder);
        try {
            document.open();
            document.add(new Paragraph("\n\n"));
            Chunk title = new Chunk("Order " + c.getClientName() + ":");
            document.add(title);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(orderTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    /**
     * Adds the desired content in the cells of the Order table
     * @param table the table in which the rows will be inserted
     */
    private void addRowsOrdersTable(PdfPTable table) {
        OrderItemBll oiBll = new OrderItemBll();
        OrderBll oBll = new OrderBll();
        ClientBll cBll = new ClientBll();
        ProductBll pBll = new ProductBll();
        for(OrderItem i : oiBll.report()){
            table.addCell(i.getOrderId()+"");
            Order o = oBll.findById(i.getOrderId());
            Client cl = cBll.findById(o.getClientId());
            table.addCell(cl.getClientName()+"");
            OrderItem it = oiBll.findById(i.getOrderId());
            Product p = pBll.findById(it.getProductId());
            table.addCell(p.getProductName());
            table.addCell(i.getOrderQuantity()+"");
            table.addCell(p.getProductPrice()+"");
            table.addCell(o.getOrderTotal()+"");
        }
    }

    /**
     * Creates and writes the content of the Order table to the pdf file
     */
    public void writeOrderTableAll(){
        try {
            this.productTable = new PdfPTable(6);
            PdfWriter.getInstance(document, new FileOutputStream("OrderTable.pdf"));
            document.open();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addOrderTableHeader(productTable);
        addRowsOrdersTable(productTable);
        try {
            document.add(new Paragraph("\n\n"));
            Chunk title = new Chunk("Orders");
            document.add(title);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("\n\n"));
            document.add(productTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }
}
