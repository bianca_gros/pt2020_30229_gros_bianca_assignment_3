package presentation;

import bll.ClientBll;
import bll.OrderItemBll;
import bll.ProductBll;
import control.Control;
import dao.AbstractDAO;
import model.Client;
import model.OrderItem;
import model.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * Class which has the job of parsing the input from the textfile given as argument
 * and also passing the informtaion forward to the Control Class
 */
public class InputParser {
    private String fileName;
    File f;
    BufferedReader buf;
    ArrayList<Object> parsed;
    Control control;

    /**
     * Constructs an InputParser being given a file name from which to read
     * @param fileName the file name from which to read
     */
    public InputParser(String fileName) {
        this.control = new Control();
        this.parsed = new ArrayList<>();
        this.fileName = fileName;
        this.f = new File(fileName);
        try {
            buf = new BufferedReader(new FileReader(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads and parses the input text and alo sends specific information to the Control class
     * @return Object
     */
    public Object read () {
            try {
                int i = 0;
                String s = "";
                while ((s = this.buf.readLine()) != null) {
                    String[] arrOfStr = s.split("[ ,:]");
                    if (arrOfStr[0].equalsIgnoreCase("Insert")) { // caz de inserare
                        if (arrOfStr[1].equalsIgnoreCase("client")) { // inserare client
                            String name = arrOfStr[3] + " " + arrOfStr[4];
                            String address = arrOfStr[6];
                            ArrayList<Object> arg = new ArrayList<>();
                            Client c = new Client(name, address);
                            arg.add(c);
                            try {
                                control.doSomething(1, arg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (arrOfStr[1].equalsIgnoreCase("product")) { // inserare produs
                            String name = arrOfStr[3];
                            int quantity = parseInt(arrOfStr[5]);
                            float price = Float.parseFloat(arrOfStr[7]);
                            ArrayList<Object> arg = new ArrayList<>();
                            Product p = new Product(name, quantity, price);
                            arg.add(p);
                            try {
                                control.doSomething(3, arg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (arrOfStr[0].equalsIgnoreCase("Delete")) { // in caz de stergere
                        if (arrOfStr[1].equalsIgnoreCase("client")) { // stergere client
                            String name = arrOfStr[3] + " " + arrOfStr[4];
                            ArrayList<Object> arg = new ArrayList<>();
                            ClientBll cBll = new ClientBll();
                            Client c = cBll.findByName(name);
                            arg.add(c);
                            try {
                                control.doSomething(2, arg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (arrOfStr[1].equalsIgnoreCase("product")) { // stergere produs
                            String name = arrOfStr[3];
                            ArrayList<Object> arg = new ArrayList<>();
                            ProductBll pBll = new ProductBll();
                            Product p = pBll.findByName(name);
                            arg.add(p);
                            try {
                                control.doSomething(4, arg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (arrOfStr[0].equalsIgnoreCase("Report")) { // in caz de raport
                        if (arrOfStr[1].equalsIgnoreCase("client")) { // raport clienti
                            OutputGenerator out = new OutputGenerator();
                            out.writeClientTable();
                        }
                        if (arrOfStr[1].equalsIgnoreCase("product")) { // raport produse
                            OutputGenerator out = new OutputGenerator();
                            out.writeProductTable();
                        }
                        if (arrOfStr[1].equalsIgnoreCase("order")) { // raport comenzi

                            OrderItemBll oiBll = new OrderItemBll();
                            List<OrderItem> orderItems = oiBll.report();
                            for(OrderItem j : orderItems){
                                OutputGenerator out = new OutputGenerator();
                                out.writeOrderTable(oiBll.findById(j.getOrderId()).getOrderId());
                            }
                            OutputGenerator out = new OutputGenerator();
                            out.writeOrderTableAll();


                        }
                    }
                    if (arrOfStr[0].equalsIgnoreCase("Order")) { // in caz de comanda
                        String name = arrOfStr[2] + " " + arrOfStr[3];
                        String produs = arrOfStr[5];
                        int quantity = parseInt(arrOfStr[7]);
                        ArrayList<Object> arg = new ArrayList<>();
                        ClientBll cBll = new ClientBll();
                        Client c = cBll.findByName(name);
                        arg.add(c);
                        ProductBll pBll = new ProductBll();
                        Product p = pBll.findByName(produs);
                        arg.add(p);
                        arg.add(quantity);
                        try {
                            control.doSomething(5, arg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    i++;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }
