package control;

import bll.ClientBll;
import bll.OrderBll;
import bll.OrderItemBll;
import bll.ProductBll;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which decides what specific operation to perform to the database
 */
public class Control {
    /**
     * Empty constructor
     */
    public Control(){
    }

    /**
     * Function to decide what specific operation to perform to the database
     * @param commandCode operation code particular to each operation
     * @param arguments an ArrayList of objects; lenght and type of elements
     * may vary as the array contains the needed elements to perform the operation specified by the commandCode
     * @throws Exception in case there are not enough products to put into a specified order
     */
    public void doSomething(int commandCode, ArrayList<Object> arguments) throws Exception {
        switch (commandCode){
            // inserare client:
            case 1:{
                ClientBll cBll = new ClientBll();
                Client client = (Client)arguments.get(0);
                cBll.insert(client);
                break;
            }
            // stergere client:
            case 2:{
                ClientBll cBll = new ClientBll();
                Client client = (Client)arguments.get(0);
                cBll.delete(client, client.getClientName());
                break;
            }
            // inserare produs:
            case 3:{
                ProductBll pBll = new ProductBll();
                Product product = (Product)arguments.get(0);
                pBll.insert(product);
                break;
            }
            // stergere produs:
            case 4:{
                ProductBll pBll = new ProductBll();
                Product product = (Product)arguments.get(0);
                pBll.delete(product, product.getProductName());
                break;
            }
            // order:
            case 5:{
                ClientBll cBll = new ClientBll();
                Client c = (Client)arguments.get(0);
                Client client = cBll.findByName(c.getClientName());
                ProductBll pBll = new ProductBll();
                Product p = (Product)arguments.get(1);
                Product product = pBll.findByName(p.getProductName());
                OrderBll oBll = new OrderBll();
                OrderItemBll oiBll = new OrderItemBll();
                int quantity = (int)arguments.get(2);
                int x = 0, ok = 0;
                if(product != null){
                    if(product.getProductQuantity() >= quantity){
                        OrderItem oi = new OrderItem(product.getProductId(), quantity);
                        oiBll.insert(oi);
                        List<OrderItem> orderItems = oiBll.report();
                        for(OrderItem f : orderItems){
                            if(f.getOrderQuantity() == oi.getOrderQuantity() && f.getProductId() == oi.getProductId()){
                                x = f.getOrderId();
                                break;
                            }
                        }
                        Order factura = new Order(x,  cBll.findByName(client.getClientName()).getClientId(), oi.getOrderQuantity() * (pBll.findById(oi.getProductId()).getProductPrice()));
                        oBll.insert(factura);
                        ok = 1;
                        if(ok == 1){
                            pBll.update(product, product.getProductQuantity() - quantity, (pBll.findByName(product.getProductName())).getProductName());
                        }
                        else{
                            oBll.delete(factura);
                            oiBll.delete(oi);
                        }
                    }
                    else{
                        throw new Exception("Not enough products");
                    }
                }
                break;
            }
            // raport client:
            case 6:{
                System.out.println("6");
                break;
            }
            // raport produs:
            case 7:{
                System.out.println("7");
                break;
            }
            // raport order (comanda):
            case 8:{
                System.out.println("8");
                break;
            }
            default:{
                throw new Exception("Wrong command code!");
            }
        }

    }
}
